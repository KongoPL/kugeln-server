class ServerHandlerManager
{
	protected static instances: ServerHandler[] = [];

	public static AddInstance( instance: ServerHandler )
	{
		this.instances.push( instance );
	}


	public static RemoveInstance( instance: ServerHandler ): boolean
	{
		for ( let c in this.instances )
		{
			if ( this.instances[c] == instance )
			{
				this.instances.splice( parseInt( c ), 1 );

				return true;
			}
		}

		return false;
	}


	public static SendBroadcast( messageName: string, data: any )
	{
		for ( let c in this.instances )
		{
			this.instances[c].SendMessage( messageName, data );
		}
	}
}