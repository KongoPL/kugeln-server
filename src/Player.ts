class Player
{
	public id: string;

	public position: Point;
	public previousPosition: Point;
	public movementDirection: Point;

	protected movementSpeed;

	private worldX: number = 0;
	private worldY: number = 0;
	private worldWidth: number;
	private worldHeight: number;

	private characterWidth: number;
	private characterHeight: number;
	private characterAnchorX: number;
	private characterAnchorY: number;

	constructor( spawnX: number, spawnY: number, id: string )
	{
		this.worldWidth = Config.getOption( "world.width" );
		this.worldHeight = Config.getOption( "world.height" );

		this.characterWidth = Config.getOption( "player.width" );
		this.characterHeight = Config.getOption( "player.height" );

		this.characterAnchorX = Config.getOption( "player.anchor.x" );
		this.characterAnchorY = Config.getOption( "player.anchor.y" );

		this.movementSpeed = Config.getOption( "player.movementSpeed" );

		this.id = id;
		this.position = new Point( spawnX, spawnY );
		this.previousPosition = new Point( spawnX, spawnY );
		this.movementDirection = new Point( 0, 0 );
	}

	public Update( deltaTime: number )
	{
		let colidesWorldbounds: any = this.ColidesWorldBounds();

		if ( colidesWorldbounds !== false )
		{
			this.position.x = this.previousPosition.x;
			this.position.y = this.previousPosition.y;
		}
		else
		{
			this.previousPosition = this.position.Copy();

			this.position.x += this.movementDirection.x * this.movementSpeed * deltaTime;
			this.position.y += this.movementDirection.y * this.movementSpeed * deltaTime;
		}
	}


	public SetMovementDirection( x: number, y: number )
	{
		this.movementDirection.SetTo( x, y );
	}


	protected ColidesWorldBounds(): boolean | ECollidedWall
	{
		let objectBounds: IObjectBounds = this.GetBounds();

		if ( objectBounds.x <= this.worldX) return ECollidedWall.LEFT;
		else if ( objectBounds.x + objectBounds.width >= this.worldX + this.worldWidth) return ECollidedWall.RIGHT;
		else if ( objectBounds.y <= this.worldY ) return ECollidedWall.TOP;
		else if ( objectBounds.y + objectBounds.height >= this.worldY + this.worldHeight ) return ECollidedWall.BOTTOM;
		else return false;
	}

	protected GetBounds(): IObjectBounds
	{
		return {
			x: this.position.x - ( this.characterWidth * this.characterAnchorX ),
			y: this.position.y - ( this.characterHeight * this.characterAnchorY ),
			width: this.characterWidth,
			height: this.characterHeight
		}
	}
}

enum ECollidedWall
{
	LEFT,
	RIGHT,
	TOP,
	BOTTOM
}

interface IObjectBounds
{
	x: number,
	y: number,

	width: number,
	height: number
}