declare function require( moduleName: string );
declare var __dirname: string;


var Fs = require( "fs" ),
	DateModule = require( "date-and-time" ),
	Http = require( "http" );
var SocketIO = require( "socket.io" )( Http );

eval( Fs.readFileSync( __dirname + "/basic/Log.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/basic/Type.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/basic/MathHelper.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/config.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/ServerHandler.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/ServerHandlerManager.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/Point.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/Game.js", "utf8" ) );
eval( Fs.readFileSync( __dirname + "/Player.js", "utf8" ) );


Log.Clear();
Log.Message( ":: Kugeln server ::" );
Log.Message( "Created by Kongo" );
Log.Message( "Version: " + Config.getOption( "version" ) );

Log.Info( "Setting up SocketIO server..." );

SocketIO.listen( Config.getOption( "server.port" ) );
SocketIO.on( 'connection', function ( client )
{
	new ServerHandler( client );
} );

Log.Success( "Server listening on " + Config.getOption( "server.port" ) );
Log.Info( "Initiating game ticking..." );

Game.Init();

Log.Success( "Ticking enabled!" );