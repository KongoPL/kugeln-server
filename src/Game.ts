class Game
{
	protected static tickInterval: number;
	protected static lastGameTick: number;

	protected static players: any = {};

	private static synchronizationTick: number = Config.getOption( "game.syncTickInterval" );
	private static tickNumber: number = 0;

	public static Init()
	{
		let _this = this;

		this.lastGameTick = new Date().getTime();
		this.tickInterval = setInterval( function ()
		{
			let currentTime = new Date().getTime();

			_this.TickGame( ( currentTime - _this.lastGameTick ) / 1000 );
			_this.lastGameTick = currentTime;
		}, 1000 / Config.getOption( "game.tickRate" ) );
	}


	public static AddPlayer( id: string, x: number, y: number )
	{
		let player: Player = new Player( x, y, id );

		this.players[id] = player;
	}


	public static ChangeMovementDirection( id: string, x: number, y: number )
	{
		if ( typeof this.players[id] !== "undefined" )
		{
			this.players[id].SetMovementDirection( x, y );
		}
	}


	public static Purge()
	{
		this.players = {};
	}


	public static TickGame( deltaTime: number )
	{
		for ( let c in this.players )
		{
			this.players[c].Update( deltaTime );
		}

		this.tickNumber++;

		if ( this.tickNumber % this.synchronizationTick == 0 )
		{
			//Send sync tick.
			ServerHandlerManager.SendBroadcast( "GameSync", this.GetSyncData() );

			this.tickNumber = 0;
		}
	}


	protected static GetSyncData(): any
	{
		let data = {};

		for ( let c in this.players )
		{
			let player = this.players[c];

			data[c] = {
				position: player.position.ToObject(),
				movementDirection: player.movementDirection.ToObject()
			};
		}

		return data;
	}

}