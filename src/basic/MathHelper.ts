class MathHelper
{
	public static RoundToPrecission( num: number, precision: number ): number
	{
		return parseFloat( ( Math.round( num * Math.pow( 10, precision ) ) * Math.pow( 10, -precision ) ).toFixed( precision ) );
	}
}