declare var DateModule: any;

class Log
{
	public static Message( ...messageObjects: any[] ): void
	{
		this.DisplayMessage( EMessageType.LOG, messageObjects );
	}


	public static Info( ...messageObjects: any[] ): void
	{
		this.DisplayMessage( EMessageType.INFO, messageObjects );
	}


	public static Success( ...messageObjects: any[] ): void
	{
		this.DisplayMessage( EMessageType.SUCCESS, messageObjects );
	}


	public static Debug( ...messageObjects: any[] ): void
	{
		this.DisplayMessage( EMessageType.DEBUG, messageObjects );
	}


	public static Clear(): void
	{
		console.clear();
	}


	public static SpaceLine( count: number = 1 ): void
	{
		for ( var i = 0; i < count; i++ )
		{
			console.log();
		}
	}


	protected static DisplayMessage( messageType: EMessageType, messageObjects: any[] ): void
	{
		let messageObjectsFinal = this.PrepareMessage( messageType, messageObjects );

		console.log.apply( this, messageObjectsFinal );
	}

	protected static PrepareMessage( messageType: EMessageType, messageObjects: any[] ): any[]
	{
		let time: string = DateModule.format( new Date(), "hh:mm:ss.SSS" ),
			logType: string = this.FormatLogType(EMessageType[messageType]);

		messageObjects.unshift( "[ " + time + " ][ " + logType + " ] " );

		return messageObjects;
	}

	protected static FormatLogType( messageType: string ): string
	{
		let longestLogName = 0;

		for ( let c in EMessageType )
		{
			if ( parseInt( c ).toString() != c && longestLogName < c.length )
			{
				longestLogName = c.length;
			}
		}

		let spacesCount = longestLogName - messageType.length;

		for ( let i = 0; i < spacesCount; i++ ) messageType = messageType + ' ';

		return messageType;
	}
}

enum EMessageType
{
	LOG,
	INFO,
	SUCCESS,
	DEBUG
}