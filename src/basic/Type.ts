class Type
{
	public static IsUndefined( variable: any ): boolean
	{
		return ( typeof variable == 'undefined' );
	}

	public static IsObject( variable: any ): boolean
	{
		return ( typeof variable == 'object' );
	}

	public static IsFunction( variable: any ): boolean
	{
		return ( typeof variable == 'function' );
	}
}