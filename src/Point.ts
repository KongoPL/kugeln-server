class Point
{
	public x;
	public y;

	constructor( x: number = 0, y: number = 0 )
	{
		this.SetTo( x, y );
	}


	public SetTo( x: number, y: number )
	{
		this.x = x;
		this.y = y;
	}


	public Magnitude()
	{
		return Math.sqrt( Math.pow( this.x, 2 ) + Math.pow( this.y, 2 ) );
	}


	public Copy(): Point
	{
		return new Point( this.x, this.y );
	}


	/**
	 * Returns data in format X: value, Y: value
	 */
	public ToObject(): any
	{
		return {
			x: this.x,
			y: this.y
		};
	}


	public ToString()
	{
		return "X=" + this.x + ";Y=" + this.y;
	}
}