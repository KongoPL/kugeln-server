class ServerHandler
{
	protected client: any;//ISocketIOClient;

	constructor( client: ISocketIOClient )
	{
		this.client = client;

		Log.Info( "New connection! Client ID:", this.client.id );

		let _this = this;

		this.client.on( "Join", function ( data ) { _this.OnJoin( data ); } );
		this.client.on( "MovementDirectionChange", function ( data ) { _this.OnMovementDirectionChange( data ); } );
		this.client.on( "Ping", function ( data ) { _this.OnPing( data ); } );
		this.client.on( "RequestServerInfo", function () { _this.OnRequestServerInfo(); } );
		this.client.on( "disconnect", function () { _this.OnDisconnect(); } );

		ServerHandlerManager.AddInstance( this );
	}


	public SendMessage( messageName: string, data: any )
	{
		this.client.emit( messageName, data );
	}


	public OnJoin( data: IOnJoin )
	{
		Game.AddPlayer( data.id, data.x, data.y );
	}


	public OnMovementDirectionChange( data: IOnMovementDirectionChange )
	{
		Game.ChangeMovementDirection( data.id, data.x, data.y );

		ServerHandlerManager.SendBroadcast( "PlayerDirectionChange", data );
	}


	public OnPing( data: number )
	{
		this.SendMessage( "Ping", data );
	}


	public OnRequestServerInfo()
	{
		this.SendMessage( "RequestServerInfo", {
			tickRate: Config.getOption( "game.tickRate" ),
			syncTick: Config.getOption( "game.syncTickInterval" )
		} );
	}


	public OnDisconnect()
	{
		Log.Info( "Connection closed by " + this.client.id );

		ServerHandlerManager.RemoveInstance( this );
		Game.Purge();
	}
}

interface IOnJoin
{
	id: string;
	x: number;
	y: number;
}

interface IOnMovementDirectionChange
{
	id: string;
	x: number;
	y: number;
}


interface ISocketIOClient
{
	/**
	 * Unique identifies for the session
	*/
	id: string;

	/**
	 * Sends message to client.
	 *
	 * eventName: Event name
	 * data: Sending data (any kind)
	 * [ack]: What to do when client answers on your message
	*/
	emit( eventName: string, data: any, ack?: ( data?: any ) => void );

	/**
	 * Adds listener for `eventName
	*/
	on( eventName: string, callback: any );
}