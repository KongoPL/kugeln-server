class Config
{
	protected static options: any = {
		version: "1.0",
		server: {
			port: 1234
		},
		player: {
			movementSpeed: 50,
			movementChangeTime: 5.5,
			width: 20,
			height: 20,
			anchor: {
				x: 0.5,
				y: 0.5
			}
		},

		game: {
			//Count of ticks per second:
			tickRate: 64,

			//After how many ticks, server have to send synchronization tick to players?
			syncTickInterval: Math.round( 64 / 8 )
		},

		world: {
			width: 800,
			height: 600
		}
	};


	public static getOption( option: string ): any
	{
		let options = option.split( "." );
		let returningValue = this.options;

		for ( let c in options )
		{
			let key = options[c];

			if ( Type.IsUndefined( returningValue[key] ) == false )
			{
				returningValue = returningValue[key];
			}
			else
			{
				return;
			}
		}

		if ( Type.IsObject( returningValue ) == false )
		{
			return returningValue;
		}
	}

	public static setOption( option: string, value: any ): any
	{
		let options = option.split( "." );
		let returningValue = this.options;

		for ( let c in options )
		{
			let key = options[c];

			if ( Type.IsUndefined( returningValue[key] ) == false )
			{
				if ( parseInt( c ) == options.length - 1 )
				{
					returningValue[key] = value;

					return true;
				}
				else
				{
					returningValue = returningValue[key];
				}
			}
			else
			{
				return false;
			}
		}
	}


	public static getOptions()
	{
		return JSON.parse( JSON.stringify( this.options ) );
	}
}