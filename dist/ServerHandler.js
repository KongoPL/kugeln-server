var ServerHandler = /** @class */ (function () {
    function ServerHandler(client) {
        this.client = client;
        Log.Info("New connection! Client ID:", this.client.id);
        /*for ( var c in this )
        {
            if ( Type.IsFunction( this[c] ) && c.substr( 0, 2 ) === "On" )
            {
                this.client.on( c.substr( 2 ), this[c] );
            }
        }*/
        var _this = this;
        this.client.on("Join", function (data) { _this.OnJoin(data); });
        this.client.on("MovementDirectionChange", function (data) { _this.OnMovementDirectionChange(data); });
        this.client.on("Ping", function (data) { _this.OnPing(data); });
        this.client.on("RequestServerInfo", function () { _this.OnRequestServerInfo(); });
        this.client.on("disconnect", function () { _this.OnDisconnect(); });
        ServerHandlerManager.AddInstance(this);
    }
    ServerHandler.prototype.SendMessage = function (messageName, data) {
        this.client.emit(messageName, data);
    };
    ServerHandler.prototype.OnJoin = function (data) {
        Game.AddPlayer(data.id, data.x, data.y);
    };
    ServerHandler.prototype.OnMovementDirectionChange = function (data) {
        Game.ChangeMovementDirection(data.id, data.x, data.y);
        ServerHandlerManager.SendBroadcast("PlayerDirectionChange", data);
    };
    ServerHandler.prototype.OnPing = function (data) {
        this.SendMessage("Ping", data);
    };
    ServerHandler.prototype.OnRequestServerInfo = function () {
        this.SendMessage("RequestServerInfo", {
            tickRate: Config.getOption("game.tickRate"),
            syncTick: Config.getOption("game.syncTickInterval")
        });
    };
    ServerHandler.prototype.OnDisconnect = function () {
        Log.Info("Connection closed by " + this.client.id);
        ServerHandlerManager.RemoveInstance(this);
        Game.Purge();
    };
    return ServerHandler;
}());
