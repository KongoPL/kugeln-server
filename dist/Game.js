var Game = /** @class */ (function () {
    function Game() {
    }
    Game.Init = function () {
        var _this = this;
        this.lastGameTick = new Date().getTime();
        this.tickInterval = setInterval(function () {
            var currentTime = new Date().getTime();
            _this.TickGame((currentTime - _this.lastGameTick) / 1000);
            _this.lastGameTick = currentTime;
        }, 1000 / Config.getOption("game.tickRate"));
    };
    Game.AddPlayer = function (id, x, y) {
        var player = new Player(x, y, id);
        //this.players.push( player );
        this.players[id] = player;
    };
    Game.ChangeMovementDirection = function (id, x, y) {
        if (typeof this.players[id] !== "undefined") {
            this.players[id].SetMovementDirection(x, y);
        }
    };
    Game.Purge = function () {
        this.players = {};
    };
    Game.TickGame = function (deltaTime) {
        for (var c in this.players) {
            this.players[c].Update(deltaTime);
        }
        this.tickNumber++;
        if (this.tickNumber % this.synchronizationTick == 0) {
            //Send sync tick.
            ServerHandlerManager.SendBroadcast("GameSync", this.GetSyncData());
            this.tickNumber = 0;
        }
    };
    Game.GetSyncData = function () {
        var data = {};
        for (var c in this.players) {
            var player = this.players[c];
            data[c] = {
                position: player.position.ToObject(),
                movementDirection: player.movementDirection.ToObject()
            };
        }
        return data;
    };
    Game.players = {};
    Game.synchronizationTick = Config.getOption("game.syncTickInterval");
    Game.tickNumber = 0;
    return Game;
}());
