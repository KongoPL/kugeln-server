var ServerHandlerManager = /** @class */ (function () {
    function ServerHandlerManager() {
    }
    ServerHandlerManager.AddInstance = function (instance) {
        this.instances.push(instance);
    };
    ServerHandlerManager.RemoveInstance = function (instance) {
        for (var c in this.instances) {
            if (this.instances[c] == instance) {
                this.instances.splice(parseInt(c), 1);
                return true;
            }
        }
        return false;
    };
    ServerHandlerManager.SendBroadcast = function (messageName, data) {
        for (var c in this.instances) {
            this.instances[c].SendMessage(messageName, data);
        }
    };
    ServerHandlerManager.instances = [];
    return ServerHandlerManager;
}());
