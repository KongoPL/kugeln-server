"use strict";
var Log = /** @class */ (function () {
    function Log() {
    }
    Log.Message = function () {
        var messageObjects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            messageObjects[_i] = arguments[_i];
        }
        this.DisplayMessage(EMessageType.LOG, messageObjects);
    };
    Log.DisplayMessage = function (messageType) {
        var messageObjects = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            messageObjects[_i - 1] = arguments[_i];
        }
        var messageObjectsFinal = this.PrepareMessage(EMessageType.LOG, messageObjects);
        console.log.call(this, messageObjectsFinal);
    };
    Log.PrepareMessage = function (messageType) {
        var messageObjects = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            messageObjects[_i - 1] = arguments[_i];
        }
        var time = DateModule.format(new Date(), "hh:mm:ss.SSS"), logType = EMessageType[messageType];
        messageObjects.unshift("[ " + time + " ][ " + logType + " ] ");
        return messageObjects;
    };
    return Log;
}());
var EMessageType;
(function (EMessageType) {
    EMessageType[EMessageType["LOG"] = 0] = "LOG";
})(EMessageType || (EMessageType = {}));
