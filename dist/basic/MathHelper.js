var MathHelper = /** @class */ (function () {
    function MathHelper() {
    }
    MathHelper.RoundToPrecission = function (num, precision) {
        return parseFloat((Math.round(num * Math.pow(10, precision)) * Math.pow(10, -precision)).toFixed(precision));
    };
    return MathHelper;
}());
