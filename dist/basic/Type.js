var Type = /** @class */ (function () {
    function Type() {
    }
    Type.IsUndefined = function (variable) {
        return (typeof variable == 'undefined');
    };
    Type.IsObject = function (variable) {
        return (typeof variable == 'object');
    };
    Type.IsFunction = function (variable) {
        return (typeof variable == 'function');
    };
    return Type;
}());
