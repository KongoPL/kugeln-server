var Log = /** @class */ (function () {
    function Log() {
    }
    Log.Message = function () {
        var messageObjects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            messageObjects[_i] = arguments[_i];
        }
        this.DisplayMessage(EMessageType.LOG, messageObjects);
    };
    Log.Info = function () {
        var messageObjects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            messageObjects[_i] = arguments[_i];
        }
        this.DisplayMessage(EMessageType.INFO, messageObjects);
    };
    Log.Success = function () {
        var messageObjects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            messageObjects[_i] = arguments[_i];
        }
        this.DisplayMessage(EMessageType.SUCCESS, messageObjects);
    };
    Log.Debug = function () {
        var messageObjects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            messageObjects[_i] = arguments[_i];
        }
        this.DisplayMessage(EMessageType.DEBUG, messageObjects);
    };
    Log.Clear = function () {
        console.clear();
    };
    Log.SpaceLine = function (count) {
        if (count === void 0) { count = 1; }
        for (var i = 0; i < count; i++) {
            console.log();
        }
    };
    Log.DisplayMessage = function (messageType, messageObjects) {
        var messageObjectsFinal = this.PrepareMessage(messageType, messageObjects);
        console.log.apply(this, messageObjectsFinal);
    };
    Log.PrepareMessage = function (messageType, messageObjects) {
        var time = DateModule.format(new Date(), "hh:mm:ss.SSS"), logType = this.FormatLogType(EMessageType[messageType]);
        messageObjects.unshift("[ " + time + " ][ " + logType + " ] ");
        return messageObjects;
    };
    Log.FormatLogType = function (messageType) {
        var longestLogName = 0;
        for (var c in EMessageType) {
            if (parseInt(c).toString() != c && longestLogName < c.length) {
                longestLogName = c.length;
            }
        }
        var spacesCount = longestLogName - messageType.length;
        for (var i = 0; i < spacesCount; i++)
            messageType = messageType + ' ';
        return messageType;
    };
    return Log;
}());
var EMessageType;
(function (EMessageType) {
    EMessageType[EMessageType["LOG"] = 0] = "LOG";
    EMessageType[EMessageType["INFO"] = 1] = "INFO";
    EMessageType[EMessageType["SUCCESS"] = 2] = "SUCCESS";
    EMessageType[EMessageType["DEBUG"] = 3] = "DEBUG";
})(EMessageType || (EMessageType = {}));
