var Config = /** @class */ (function () {
    function Config() {
    }
    Config.getOption = function (option) {
        var options = option.split(".");
        var returningValue = this.options;
        for (var c in options) {
            var key = options[c];
            if (Type.IsUndefined(returningValue[key]) == false) {
                returningValue = returningValue[key];
            }
            else {
                return;
            }
        }
        if (Type.IsObject(returningValue) == false) {
            return returningValue;
        }
    };
    Config.setOption = function (option, value) {
        var options = option.split(".");
        var returningValue = this.options;
        for (var c in options) {
            var key = options[c];
            if (Type.IsUndefined(returningValue[key]) == false) {
                if (parseInt(c) == options.length - 1) {
                    returningValue[key] = value;
                    return true;
                }
                else {
                    returningValue = returningValue[key];
                }
            }
            else {
                return false;
            }
        }
    };
    Config.getOptions = function () {
        return JSON.parse(JSON.stringify(this.options));
    };
    Config.options = {
        version: "1.0",
        server: {
            port: 1234
        },
        player: {
            movementSpeed: 50,
            movementChangeTime: 5.5,
            width: 20,
            height: 20,
            anchor: {
                x: 0.5,
                y: 0.5
            }
        },
        game: {
            //Count of ticks per second:
            tickRate: 64,
            //After how many ticks, server have to send synchronization tick to players?
            syncTickInterval: Math.round(64 / 8)
        },
        world: {
            width: 800,
            height: 600
        }
    };
    return Config;
}());
