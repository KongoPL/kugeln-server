var Player = /** @class */ (function () {
    function Player(spawnX, spawnY, id) {
        this.worldX = 0;
        this.worldY = 0;
        this.worldWidth = Config.getOption("world.width");
        this.worldHeight = Config.getOption("world.height");
        this.characterWidth = Config.getOption("player.width");
        this.characterHeight = Config.getOption("player.height");
        this.characterAnchorX = Config.getOption("player.anchor.x");
        this.characterAnchorY = Config.getOption("player.anchor.y");
        this.movementSpeed = Config.getOption("player.movementSpeed");
        this.id = id;
        this.position = new Point(spawnX, spawnY);
        this.previousPosition = new Point(spawnX, spawnY);
        this.movementDirection = new Point(0, 0);
    }
    Player.prototype.Update = function (deltaTime) {
        var colidesWorldbounds = this.ColidesWorldBounds();
        if (colidesWorldbounds !== false) {
            this.position.x = this.previousPosition.x;
            this.position.y = this.previousPosition.y;
            //Log.Debug( "Magnitude: " + this.movementDirection.Magnitude() );
            /*if ( this.movementDirection.Magnitude() > 0 )
            {
                this.movementDirection.SetTo( 0, 0 ); //Server shouldn't ingerate into player's movement direction.
            }*/
        }
        else {
            this.previousPosition = this.position.Copy();
            this.position.x += this.movementDirection.x * this.movementSpeed * deltaTime;
            this.position.y += this.movementDirection.y * this.movementSpeed * deltaTime;
        }
        /*Log.Debug( this.id,
            "My coordinates:", Math.round( this.position.x ), Math.round( this.position.y ),
            "My direction:", MathHelper.RoundToPrecission( this.movementDirection.x, 4 ), MathHelper.RoundToPrecission( this.movementDirection.y, 4 )
        );*/
    };
    Player.prototype.SetMovementDirection = function (x, y) {
        this.movementDirection.SetTo(x, y);
    };
    Player.prototype.ColidesWorldBounds = function () {
        var objectBounds = this.GetBounds();
        if (objectBounds.x <= this.worldX)
            return ECollidedWall.LEFT;
        else if (objectBounds.x + objectBounds.width >= this.worldX + this.worldWidth)
            return ECollidedWall.RIGHT;
        else if (objectBounds.y <= this.worldY)
            return ECollidedWall.TOP;
        else if (objectBounds.y + objectBounds.height >= this.worldY + this.worldHeight)
            return ECollidedWall.BOTTOM;
        else
            return false;
    };
    Player.prototype.GetBounds = function () {
        return {
            x: this.position.x - (this.characterWidth * this.characterAnchorX),
            y: this.position.y - (this.characterHeight * this.characterAnchorY),
            width: this.characterWidth,
            height: this.characterHeight
        };
    };
    return Player;
}());
var ECollidedWall;
(function (ECollidedWall) {
    ECollidedWall[ECollidedWall["LEFT"] = 0] = "LEFT";
    ECollidedWall[ECollidedWall["RIGHT"] = 1] = "RIGHT";
    ECollidedWall[ECollidedWall["TOP"] = 2] = "TOP";
    ECollidedWall[ECollidedWall["BOTTOM"] = 3] = "BOTTOM";
})(ECollidedWall || (ECollidedWall = {}));
