var Point = /** @class */ (function () {
    function Point(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.SetTo(x, y);
    }
    Point.prototype.SetTo = function (x, y) {
        this.x = x;
        this.y = y;
    };
    Point.prototype.Magnitude = function () {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    };
    Point.prototype.Copy = function () {
        return new Point(this.x, this.y);
    };
    /**
     * Returns data in format X: value, Y: value
     */
    Point.prototype.ToObject = function () {
        return {
            x: this.x,
            y: this.y
        };
    };
    Point.prototype.ToString = function () {
        return "X=" + this.x + ";Y=" + this.y;
    };
    return Point;
}());
